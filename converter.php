<?php

function openCsv($file) {
	if (($handle = fopen($file, 'r')) !== FALSE) { // Check the resource is valid
		while (($data = fgetcsv($handle, 100000, ",")) !== FALSE) { // Check opening the file is OK!
			$array[] = $data; // Array
		}
		fclose($handle);
	}
	return $array;
}

echo "CSV converter";
echo "\n";

include('config.php');

$csv_array = openCsv($source_csv);
$config_array = openCsv($config_csv);

$fields_array = array();
//$i = 0;
$line_number = 0;
// Convert replace rules into arrays in the config array
foreach($config_array as $key => $value) {
	// We leave out the first line with the labels
	if($line_number != 0) {
		// For all columns
		for($i = 0; $i <= 5; $i++) {
			// If the cell contentent is not a replace rule
			if($i != 5) {
				$fields_array[$key-1][$i] = $value[$i];
			} else {
				if($value[$i-1] == 'Replace' or $value[$i-1] == 'Replace multiple') {
					// Let's turn the replace rule into an array
					unset($replace_array);
					$replace_array = array();
					$replace = explode(",", $value[$i]);
					foreach($replace as $replace_value) {
						$replace_2 = explode(":", $replace_value);
						$replace_array[substr($replace_2[0], 1, -1)] = substr($replace_2[1], 1, -1);
					}
					$fields_array[$key-1][$i] = $replace_array;
				} else {
					// If there is no replace rule, this cell is set empty
					$fields_array[$key-1][$i] = $value[$i];
				}
			}
		}
	}
	$line_number++;
}

//print_r($fields_array);
$m2_array = array();

$i = 0;
// For each line of the source array
foreach ($csv_array as $key => $value) {
	if($i>0) {
		// For each cell of the source line
		foreach($value as $key2 => $value2) {

			// If the column is to be used and the new column is not empty, otherwise the cell will be left out
			//print_r($fields_array[$key2]);
			if($fields_array[$key2][0] == 1 and $fields_array[$key2][2] != '') {
				// Set the new column number as the $index
				$index = $fields_array[$key2][2];
				// If there is no overwrite, replace or special rule
				if($fields_array[$key2][4] == '') {
					$m2_array[$i][$index] = trim($value2);
				} else {
					if($fields_array[$key2][4] == 'Divide by') {
						if(trim($value2) != '') {
							$m2_array[$i][$index] = trim($value2) / trim($fields_array[$key2][5]);
						} else {
							$m2_array[$i][$index] = '';
						}
					}
					if($fields_array[$key2][4] == 'Prefix') {
						if(trim($value2) != '') {
							$m2_array[$i][$index] = trim($fields_array[$key2][5]).trim($value2);
						} else {
							$m2_array[$i][$index] = '';
						}
					}
					if($fields_array[$key2][4] == 'Suffix') {
						if(trim($value2) != '') {
							$m2_array[$i][$index] = trim($value2).trim($fields_array[$key2][5]);
						} else {
							$m2_array[$i][$index] = '';
						}
					}
					if($fields_array[$key2][4] == 'Overwrite') {
						$m2_array[$i][$index] = trim($fields_array[$key2][5]);
					}
					// If there is a replace with multiple values
					if($fields_array[$key2][4] == 'Replace multiple') {
						$conversion_array = $fields_array[$key2][5];
						$source_array = explode(",", $value2);
						$result_text = '';
						$first = true;
						foreach($source_array as $source_array_value) {
							// If there is such index defined, we use the associated value
							if(isset($conversion_array[$source_array_value])) {
								if($first == true) {
									$first = false;
								} else {
									$result_text .= ',';
								}
								$result_text .= trim($conversion_array[$source_array_value]);
							// If there is no such index, we use the original value, no overwrite
							//} else {
							//	$result_text .= trim($source_array_value);
							}
						}
						$m2_array[$i][$index] = $result_text;
						//print_r($source_array);

					}
					// If there is a replace with a single value
					if($fields_array[$key2][4] == 'Replace') {
						$conversion_array = $fields_array[$key2][5];
						// If there is such index defined, we use the associated value
						if(isset($conversion_array[$value2])) {
							$m2_array[$i][$index] = trim($conversion_array[$value2]);
						// If there is no such index, we use the original value, no overwrite
						} else {
							$m2_array[$i][$index] = trim($value2);
						}
					}
					// If there is a special rule
					if($fields_array[$key2][4] == 'Unique') {
						// If there is no unique array for this index, create it
						if(!isset($unique[$key2])) {
							$unique[$key2] = array();
						}
						// If the value is present in the unique array, add the value with an extra character
						if(in_array(trim($value2), $unique[$key2])) {
							if(!isset($duplicate[trim($value2)])) {
								$duplicate[trim($value2)] = 1;
							}
							$m2_array[$i][$index] = trim($value2).'-'.$duplicate[trim($value2)];
							$duplicate[trim($value2)]++;
						} else {
							// Otherwise just add the value
							$m2_array[$i][$index] = trim($value2);
							$unique[$key2][] = trim($value2);
						}
					}
				}
			}
		}
	}
	$i++;
}

// Exception to save only products with these sku-s:
$new_sku = array(
	"gavekort-valgfritt", "120034-1", "120034-3", "120034-6", "120034-7", "120034-9", "120034-10", "120034-11", "120034-12", "151294-4", "152293", "152979", "120034-17", "GSB0011", "GSB0052-253", "GSB0056", "Murad3423", "OH-111", "CD873", "BN83", "SH80", "OFRA11", "BM67677", "vh677677", "ESSIEEXPR1005", "L100299", "GLOD20201", "GLOD20203", "MAY700", "PE32", "S10", "58383837", "345977379", "584737379", "38277666", "747499", "PF6", "7848387", "93878", "89487", "987873", "839438", "874743", "984898", "267347", "673763", "8757873", "3438753", "34367", "376478", "3647879", "7843651", "2734671", "2734672", "736733", "3267436", "4875783", "28357", "463771", "463775", "376421", "34985", "LE45268", "SUN4476", "AB1853", "AB1853-3", "AB1853-6", "AN9284", "VO7374", "VO7858", "VO75899", "VO73658", "FI1", "FI2", "FI3", "FI4", "FI5", "FI6", "FI7", "FI8", "FI9", "FI10", "FI11", "FI12", "FI13", "FI14", "FI15", "FI17", "FI20", "FI21", "FI22", "FI23", "FI24", "FI25", "FI26", "FI27", "FI28", "FI29", "FI30", "FI31", "FI32", "FI33", "FI34", "FI35", "FI36", "FI37", "FI38", "FI39", "FI40", "FI41", "FI42", "FI43", "FI44", "FI45", "FI46", "FI47", "FI48", "FI52", "FI53", "FI54", "FI55", "FI56", "BS1", "BS2", "BS4", "BS5", "BS6", "BS8", "BS10", "BS11", "BS13", "BS14", "BS16", "BS17", "BS18", "BS19", "BS20", "BS21", "BS22", "BS23", "BS24", "BS25", "BS26", "BS27", "BS28", "RMS127", "RMS128", "CA98738", "GLOD4888", "GLOD8588", "GLOD8779", "GLOD7758", "GLOD2342", "ICON75738", "ICON7484", "CTJ6", "CTJ7", "JULEN1", "JULEN3", "JULEN4", "JULEN5", "JULEN7", "JULEN8", "JULEN9", "JULEN12", "JULEN13", "JULEN14", "JULEN15", "KO7478", "KO94899", "32778", "84757", "455679", "887478", "252677", "655636", "2734586", "89777", "877438", "7573799", "737766", "834747", "7857878", "8377473", "7573668", "878437", "5784578", "949048", "973637", "8945747", "98773987", "7843467", "8747566", "PHY77848", "BR10", "BR10-1", "BR10-2", "PHY7438", "PHY73490", "PHY109-1", "NC566", "NC9839", "NC66746", "NC8948", "NC857", "NC654", "NC26468", "NC6468", "NC2376", "83777", "45343", "5887", "365676", "485779", "378457", "JULEN17", "JULEN18", "JULEN19", "JULEN21", "JULEN23", "JULEN24", "JULEN25", "JULEN26", "JULEN27", "JULEN28", "JULEN29", "JULEN33", "JULEN34", "JULEN36", "JULEN37", "JULEN40", "JULEN42", "JULEN44", "JULEN47", "JULEN49", "GIFT2000", "HAIR1", "HAIR3", "HAIR5", "HAIR7", "HAIR9", "HAIR11", "HAIR12", "HAIR13", "HAIR14", "HAIR15", "LAN1", "LAN2", "LAN3", "LAN4", "LAN5", "LAN6", "LAN7", "LAN8", "LAN9", "LAN10", "LAN11", "LAN12", "LAN13", "LG13", "LG54", "LG82", "FI61", "FI62", "FI63", "FI67", "LU1919", "LU1919-3", "LU1919-4", "LU1919-5", "LU883", "LU883-2", "LU883-3", "LU883-4", "LU883-5", "LU7728", "LU7728-1", "LU7728-2", "LU7728-3", "LU7728-4", "LU7728-5", "LU7728-6", "LU399", "LU399-1", "LU399-2", "LU399-3", "LU399-4", "LU399-5", "LU399-6", "LU3627", "LU3627-1", "LU3627-2", "LU3627-3", "LU3627-4", "LU3627-5", "LU3627-6", "LU2663", "LU2663-1", "LU2663-2", "LU2663-3", "LU2663-4", "LU2663-5", "LU2663-6", "LU7747", "LU7747-1", "LU7747-2", "LU7747-3", "LU7747-4", "LU7747-5", "LU7747-6", "CA100", "CA200", "CA300", "CA400", "J500", "RC100", "RC200", "RC300", "RC400", "WE200", "WE300", "WE400", "WE600", "DR60", "DR61", "OR100", "OR200", "OR300", "OR400", "OR500", "OR600", "OR700", "OR800", "OR900", "OR1000", "OR1100", "OR1200", "OR1300", "MM100", "CC100", "LL100", "PA100", "PA1000", "PA10000", "OR2398", "OR5787", "OR9397", "FU10", "FU20", "FU30", "FU40", "LOR389", "VO3489", "OLE400", "15129410", "BT100", "BT100-1", "BT100-2", "RE4877", "RE8830", "RE6477", "ABH1000", "ABH1000-1", "ABH1000-2", "ABH1000-3", "ABH1100", "ABH1100-3", "MAY0029", "MAY0029-2", "MAY0029-3", "LYS100", "OR19489", "MIS0002", "152060-2", "MA2987", "MA74878", "384788", "PRO28774", "547687", "89347", "2348", "3472", "1877", "7877", "3894", "150689", "374687", "346760", "237456", "BM43553", "BM193", "BM1939", "BM75748", "BM3938", "23847", "75698", "134857", "94588", "7485890", "234987", "83475", "765789", "478887", "39458", "GLOD3030", "45789", "348788", "CAU200", "CAU400", "465672", "576778", "345423", "345789", "34375", "93857", "454766", "456788", "234866", "435777", "567899", "347856", "345600", "88048", "5553039", "5553039-1", "5553039-2", "5553039-5", "5553039-6", "5553039-7", "SU99910-1", "SU99910-2", "23787", "23787-1", "23787-2", "23787-3", "246677", "246677-1", "246677-2", "246677-3", "246677-4", "5656", "5656-1", "5656-2", "5656-4", "5656-5", "5656-6", "5656-7", "5656-8", "52422-17", "52422-18", "BM93945", "538647", "53220", "LOR4440-2", "LOR4440-1", "LOR4440-3", "LOR4440-4", "LOR4440-5", "LOR328-1", "LOR328-2", "LOR328-3", "LOR328-4", "LOR328-5", "348578", "938778", "46276", "454590", "347882", "547878", "4878900", "56483", "340596", "3476669", "43787", "39388", "238746", "OFRA471", "OFRA142", "BM824", "BM824-1", "BM824-2", "BM824-3", "BM824-4", "BM824-5", "BM824-6", "BM741", "BM742", "BM743", "BM744", "BM745", "BM272", "BM273", "BM274", "BM275", "BM276", "BM277", "BM278", "17402-31", "3459890", "359388", "5849788", "235479", "354878", "58983", "549879", "453798", "52422-11", "K2911021", "K2911022", "102934", "102933", "94838", "945628", "249857", "93847", "93742", "95847", "91374", "96435", "98746", "94636", "97846", "93743", "9585883", "MU2148", "MU477", "MU654", "MURAD-80000", "MURAD-80280", "MURAD-80575", "MU3218", "52395", "MU9348"
);


// Get the column names for the first line
$first_line = array();
reset($fields_array);
foreach($fields_array as $key => $value) {
	if($value[0] == 1 and $value[2] != '') {
		$first_line[$value[2]] = $value[3];
	}
}
ksort($first_line);
echo "Columns in the export:\n";
print_r($first_line);

$files = 0;
// Create and open the export file for writing
$fp = fopen('exported/'.$export_prefix.'-'.date("Y-m-d--H-i-s").'--'.$files.'.csv', 'w');
// Add the first line
fputcsv($fp, $first_line);
// Add the rest of the lines
$i = 1;
foreach ($m2_array as $fields) {
	if($i > 0 and $i % $split_at == 0) {
		fclose($fp);
		$files++;
		$fp = fopen('exported/'.$export_prefix.'-'.date("Y-m-d--H-i-s").'--'.$files.'.csv', 'w');
		fputcsv($fp, $first_line);
	}
	if($i != 0) {
		ksort($fields);
    	fputcsv($fp, $fields);
		/*if(in_array($fields[1], $new_sku)) {
    		fputcsv($fp, $fields);

		} else {
			$i--;
		}*/
	}
	$i++;
}
// Close the export file
fclose($fp);