# CSV Converter

CSV Converter is a PHP CLI script to help convert between different CSV files.

## Prerequisites

You need PHP CLI to run this script.
On Ubuntu based distributions use this command in the terminal:
```bash
sudo apt install php-cli
```
On Windows there are multiple ways to install PHP.
1. Use WSL and the same command as above.
2. Install PHP and follow these instructions: https://www.sunant.com/running-php-from-windows-command-line/
3. Install XAMPP and follow these instructions: https://fellowtuts.com/php/run-php-from-command-line-in-windows-and-xampp/


## Installation

Download the script as a zip file or through git.

```bash
git clone https://tenchi39@bitbucket.org/tenchi39/csv-converter.git
```

## Usage

To convert a source CSV file to the destination format, you need a translation key in the form of a CSV. A sample of both files is included, so if you want to try the converter, all you need to do is to run the following command in the terminal:

```bash
php ./converter.php
```
Later on you should place your source and config csv files into the /source folder and modify the config.php file to update the path for the source csv and conversion rules. 

## Configuration

conversion.csv contains all the rules for transforming your source csv into the destination csv. The columns of this file are as follows:

- Use in conversion: Set to 1 if you need this column in the destination csv file.
- Title in old csv: List all the column names of the source csv file in this column vertically.
- Order in new csv: You can set the column order in the destination file. Be sure to start with 1 and not 0!
- Title in new csv: You can change the column name in the destination file if you want to. Do not leave empty if you are using this column. 
- Transformation: If you simply want to copy the values, leave empty. Other options are:
	- Overwrite: Overwrites the value to that which is place in the next column (Transformation value)
	- Replace: Replaces the value according to an array-like association. See the provided example for the proper format.
	- Replace multiple: Replaces comma-seperated values according to an array-like association. Same as Replace, but with a list.
	- Unique: If the current value was already processed, an increasing index is added as a suffix to the value to make it unique.
	- Prefix: The content of the next column is added in front of the value.
	- Suffix: The content of the next column is added after the value. 
	- Divide by: Diviede the value by the vaue given the Transformation value column.
- Transformation value: Only needed in case of Overwrite, Replace, Prefix, Suffix and Divide by transformations, otherwise leave empty.


## License
[MIT](https://choosealicense.com/licenses/mit/)
